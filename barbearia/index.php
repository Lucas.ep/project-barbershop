	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
	<link rel='stylesheet' type='text/css' href=''>
	<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
	<link rel='stylesheet href='css/princessSophia.css'/>
	<link rel='stylesheet' href='css/swiper.min.css'>
	<link rel='stylesheet' type='text/css' href='css/style.css'>
	<style class="">[class^="swiper-button-"], .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet, .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet::before {
	  transition: all .3s ease;
	}

	*, *:before, *:after {
	  box-sizing: border-box;
	  margin: 0;
	  padding: 0;
	}

	.swiper-container {
	  width: 100%;
	  height: 30vw;
	  transition: opacity .6s ease;
	}
	.swiper-container.swiper-container-coverflow {
	  padding-top: 2%;
	}
	.swiper-container.loading {
	  opacity: 0;
	  visibility: hidden;
	}
	.swiper-container:hover .swiper-button-prev,
	.swiper-container:hover .swiper-button-next {
	  -webkit-transform: translateX(0);
	          transform: translateX(0);
	  opacity: 1;
	  visibility: visible;
	}

	.swiper-slide {
	  background-position: center;
	  background-size: cover;
	}
	.swiper-slide .entity-img {
	  display: none;
	}
	.swiper-slide .content {
	  position: absolute;
	  top: 40%;
	  left: 0;
	  width: 50%;
	  padding-left: 5%;
	  color: #fff;
	}
	.swiper-slide .content .title {
	  font-size: 2.6em;
	  font-weight: bold;
	  margin-bottom: 30px;
	}
	.swiper-slide .content .caption {
	  display: block;
	  font-size: 13px;
	  line-height: 1.4;
	}

	[class^="swiper-button-"] {
	  width: 44px;
	  opacity: 0;
	  visibility: hidden;
	}

	.swiper-button-prev {
	  -webkit-transform: translateX(50px);
	          transform: translateX(50px);
	}

	.swiper-button-next {
	  -webkit-transform: translateX(-50px);
	          transform: translateX(-50px);
	}

	.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet {
	  margin: 0 9px;
	  position: relative;
	  width: 12px;
	  height: 12px;
	  background-color: #fff;
	  opacity: .4;
	}
	.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet::before {
	  content: '';
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  width: 18px;
	  height: 18px;
	  -webkit-transform: translate(-50%, -50%);
	          transform: translate(-50%, -50%);
	  border: 0px solid #fff;
	  border-radius: 50%;
	}
	.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet:hover, .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet.swiper-pagination-bullet-active {
	  opacity: 1;
	}
	.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet.swiper-pagination-bullet-active::before {
	  border-width: 1px;
	}

	@media (max-width: 1180px) {
	  .swiper-slide .content .title {
	    font-size: 25px;
	  }
	  .swiper-slide .content .caption {
	    font-size: 12px;
	  }
	}
	@media (max-width: 1023px) {
	  .swiper-container {
	    height: 85vw;
	  }
	  .swiper-container.swiper-container-coverflow {
	    padding-top: 0;
	  }
	}
</style>

<div class="">	
		<div id='primeiro'>
			<div class='uk-position-relative'>
			<div class='uk-height-large uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light' data-src='imgs/SOBRE2.jpg' style="height: 650px;" uk-img><br>
  				<h1>Barber Shop of Joseph</h1>
			</div>
		    <div class='uk-position-top'>
		    	<div uk-sticky='sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar'>
		        <nav class='uk-navbar-container uk-navbar-transparent' uk-navbar id='menu'>
		            <div class='uk-navbar-center'>
		                <ul class='uk-navbar-nav'>
		                    <li><a href='#'>Home</a></li>
		                    <li><a href='#'>Cortes</a></li>
		                    <li><a href='#'><img id='logu' src='imgs/fu.png'/></a></li>
		                    <li><a href='#'>Agenda</a></li>
		                    <li><a href='#'>Contatos</a></li>
		                </ul>
		            </div>
		        </nav>
		      </div>
		   </div>
		</div>
	</div>

		<div class='parallax-window' data-parallax='scroll' data-image-src='imgs/tu.jpg'></div>
	
	<div uk-grid id='segunda'>
		<div class=''><br><br><br><br>
		<center><h1 style='color: white'>Our History</h1><br><br><br>
			<div style="" class="uk-child-width-1-3@s uk-text-center" uk-grid>
			   	<div>
			        <div class="uk-card uk-card-default">
			            <div class="uk-card-media-top">
			                <img src="imgs/SOBRE2.jpg" alt="">
			            </div>
			            <div class="uk-card-body">
			                <h3 class="uk-card-title">Projeto BarberShop</h3>
			                <p>BarberShop BarberShop BarberShop BarberShop BarberShop BarberShop</p><br>
			                <a href="http://localhost:8000/" class="uk-button uk-button-default uk-button-large uk-width-auto@s" data-uk-icon="arrow-right" title="Learn More">Ver Projeto</a>
			            </div>
			       	</div>
		    	</div>
		    	<div>
			        <div class="uk-card uk-card-default">
			            <div class="uk-card-media-top">
			                <img src="imgs/SOBRE2.jpg" alt="">
			            </div>
			            <div class="uk-card-body">
			                <h3 class="uk-card-title">Projeto BarberShop</h3>
			                <p>BarberShop BarberShop BarberShop BarberShop BarberShop BarberShop</p><br>
			                <a href="http://localhost:8000/" class="uk-button uk-button-default uk-button-large uk-width-auto@s" data-uk-icon="arrow-right" title="Learn More">Ver Projeto</a>
			            </div>
			       	</div>
		    	</div>
		    	<div>
			        <div class="uk-card uk-card-default">
			            <div class="uk-card-media-top">
			                <img src="imgs/SOBRE2.jpg" alt="">
			            </div>
			            <div class="uk-card-body">
			                <h3 class="uk-card-title">Projeto BarberShop</h3>
			                <p>BarberShop BarberShop BarberShop BarberShop BarberShop BarberShop</p><br>
			                <a href="http://localhost:8000/" class="uk-button uk-button-default uk-button-large uk-width-auto@s" data-uk-icon="arrow-right" title="Learn More">Ver Projeto</a>
			            </div>
			       	</div>
		    	</div>
		    </div>	  
		</div></center>
	</div>

	<div id="terceira">
			<center><h1 style='color: white;'>Our Cuts</h1></center><br><br>
				<section class="swiper-container loading">
				  <div class="swiper-wrapper">
				    <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				    <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				    <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				    <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				    <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				    <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				     <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				     <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				    <div class="swiper-slide" style="background-image:url(imgs/SOBRE2.jpg)">
				      <img src="img/SOBRE2.jpg" class="entity-img" />
				      <div class="content">
				        <p class="title" data-swiper-parallax="-30%" data-swiper-parallax-scale=".7">Joseph</p>
				        <span class="caption" data-swiper-parallax="-20%">Projeto</span>
				      </div>
				    </div>
				  </div>
			  
				  <div class="swiper-pagination"></div>
				  <div class="swiper-button-prev swiper-button-white"></div>
				  <div class="swiper-button-next swiper-button-white"></div>
				</section>
		</div>

		<div class='uk-light wrap uk-background-norepeat uk-background-cover uk-background-center-center uk-cover-container uk-background-secondary parallax-window' data-parallax='scroll' data-image-src='imgs/laaa.jpg'>
			<div  class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport uk-position-z-index uk-position-relative">
				<center><h1 id="a">Our Opening Hours</h1><br><br><br>
					<div class="uk-child-width-1-6@m uk-grid-small uk-grid-match" uk-grid uk-scrollspy="cls: uk-animation-fade; target: .uk-card; delay: 300; repeat: true" uk-grid>
				    	<div>
				        	<div style="background-color: black;opacity: 0.7!important;height: 150px;" class="uk-card uk-card-body">
				            	<h3 class="uk-card-title">Segunda</h3>
				            	<p>06:00-19:00</p>
				        	</div>
				    	</div>
					   <div>
					        <div style="background-color: black;opacity: 0.7!important;height: 150px;" class="uk-card uk-card-body">
					            <h3 class="uk-card-title">Terça</h3>
					            <p>06:00-19:00</p>
					        </div>
					    </div>
					   <div>
					        <div style="background-color: black;opacity: 0.7!important;height: 150px;" class="uk-card uk-card-body">
					            <h3 class="uk-card-title">Quarta</h3>
					            <p>06:00-19:00</p>
					        </div>
					    </div>
					    <div>
					        <div style="background-color: black;opacity: 0.7!important;height: 150px;" class="uk-card uk-card-body">
					            <h3 class="uk-card-title">Quinta</h3>
					            <p>06:00-19:00</p>
					        </div>
					    </div>
					    <div>
					        <div style="background-color: black;opacity: 0.7!important;height: 150px;" class="uk-card uk-card-body">
					            <h3 class="uk-card-title">Sexta</h3>
					            <p>06:00-19:00</p>
					        </div>
					    </div>
					    <div>
					        <div style="background-color: black;opacity: 0.7!important;height: 150px;" class="uk-card uk-card-body">
					            <h3 class="uk-card-title">Sábado</h3>
					            <p>06:00-19:00</p>
					        </div>
					    </div>
					</div></center>
				</div>
			</div>
			<div id="quarta">
				<div class="uk-background-center-center uk-cover-container uk-background-secondary">
					<img class="uk-blend-multiply" data-srcset="imgs/EI.jpg" data-src="imgs/tabe.jpg" alt="" data-uk-cover data-uk-img
					><br><br><br>
					<center><h1 style="color: #FFFFFFFF;"><br>Ours Products</h1></center><br><br><br>
					<div  class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport uk-position-z-index uk-position-relative">
						<div uk-slider="center: true" style="width: 100%;">
						    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
						        <ul class="uk-slider-items uk-child-width-1-3@s uk-grid">
						            <li>
						                <div class="uk-card">
						                    <div class="uk-card-media-top">
						                        <img src="imgs/aa.png" alt="">
						                    </div>
						                    <div class="uk-card-body">
						                        <h3 class="uk-card-title">Produtos</h3>
						                        <p>produtos</p>
						                    </div>
						                </div>
						            </li>
						             <li>
						                <div class="uk-card">
						                    <div class="uk-card-media-top">
						                        <img src="imgs/aaa.png" alt="">
						                    </div>
						                    <div class="uk-card-body">
						                        <h3 class="uk-card-title">Produtos</h3>
						                        <p>produtos</p>
						                    </div>
						                </div>
						            </li>
						             <li>
						                <div class="uk-card">
						                    <div class="uk-card-media-top">
						                        <img src="imgs/n.png" alt="">
						                    </div>
						                    <div class="uk-card-body">
						                        <h3 class="uk-card-title">Produtos</h3>
						                        <p>produtos</p>
						                    </div>
						                </div>
						            </li>
						             <li>
						                <div class="uk-card">
						                    <div class="uk-card-media-top">
						                        <img src="imgs/aaaaa.png" alt="">
						                    </div>
						                    <div class="uk-card-body">
						                        <h3 class="uk-card-title">Produtos</h3>
						                        <p>produtos</p>
						                    </div>
						                </div>
						            </li>
						             <li>
						                <div class="uk-card">
						                    <div class="uk-card-media-top">
						                        <img src="imgs/af.png" alt="">
						                    </div>
						                    <div class="uk-card-body">
						                        <h3 class="uk-card-title">Produtos</h3>
						                        <p>produtos</p>
						                    </div>
						                </div>
						            </li>
						        </ul><br><br>

						        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
						        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

						    </div>

						    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

						</div>
					</div>
				</div>
			</div>

			<div id="quinto">
				<div class="uk-background-center-center uk-cover-container uk-background-secondary">
					<img class="uk-blend-multiply" data-srcset="imgs/own.jpg" data-src="imgs/tabe.jpg" alt="" data-uk-cover data-uk-img
					>
					<center><h1 style="color: white;margin-top: 100px;" >Ours Prices</h1></center>
					<div  class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport uk-position-z-index uk-position-relative">
						<table id="tabela" class="uk-table uk-table-divider" >
							<tr>
								<th style="float: left;">Corte</th>
								<th style="text-align: center;">nha</th>
								<th>Preço</th>
							</tr>
							<tr>
								<td>Vinking</td>
								<td>aa</td>
								<td>R$250,00</td>
							</tr>
							<tr>
								<td>Vinking</td>
								<td>aa</td>
								<td>R$250,00</td>
							</tr>
							<tr>
								<td>Vinking</td>
								<td>aa</td>
								<td>R$250,00</td>
							</tr>
							<tr>
								<td>Vinking</td>
								<td>aa</td>
								<td>R$250,00</td>
							</tr>
							<tr>
								<td>Vinking</td>
								<td>aa</td>
								<td>R$250,00</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<div id="sexta"><br><br><br><br>
				<center><h1 style='color: white;'>Our Tatoos</h1><br><br><br>
				<div id="slides">
				    <img src="imgs/SOBRE2.jpg">
				    <img src="imgs/SOBRE2.jpg">
				    <img src="imgs/SOBRE2.jpg">
				    <img src="imgs/SOBRE2.jpg">
				    <img src="imgs/SOBRE2.jpg">
				</div></center><br><br><br><br>

				<center><h1 style='color: white'>Tutorials</h1><br><br><br>
				<div class="uk-child-width-1-2@s uk-text-center" uk-grid>
					    <div class="uk-card-media-left uk-cover-container">
							<video src="https://yootheme.com/site/images/media/yootheme-pro.mp4" controls playsinline  uk-video></video>
					    </div>
					    <div>
					        <div class="uk-card-body"><br>
					            <h3 class="uk-card-title">Video</h3>
					            <p>Famoso Texto da Tinker Bell</p>
					        </div>
					    </div>
					    <div class="uk-flex-last@s uk-card-media-right uk-cover-container"><br><br>
							<video src="https://yootheme.com/site/images/media/yootheme-pro.mp4" controls playsinline  uk-video></video>
					    </div>
					    <div>
					        <div class="uk-card-body"><br>
					            <h3 class="uk-card-title">Video</h3>
					            <p>Famoso Texto da Tinker Bell</p>
					        </div>
					    </div>
				</div>

			  </div></center>
			</div>
			<footer style="background-image: url(imgs/eee.jpg);background-size: cover;" class="uk-section uk-section-secondary uk-padding-remove-bottom">
			<div class="uk-container">
				<div class="uk-grid uk-grid-large" data-uk-grid>
					<div class="uk-width-1-2@m">
						<h5>CONTATOS</h5>
						<p></p>
						<div>
							<a href="" class="uk-icon-button" data-uk-icon="twitter"></a>Twitter<br><br>
							<a href="" class="uk-icon-button" data-uk-icon="facebook"></a>Facebook<br><br>
							<a href="" class="uk-icon-button" data-uk-icon="instagram"></a>Instagram
						</div>
					</div>
					<div class="uk-width-1-6@m">
						<h5>PÁGINAS</h5>
						<ul class="uk-list">
							<li>Entrar</li>
							<li>Cadastrar</li>
							<li>Sobre</li>
						</ul>
					</div>
					
					
				</div>
			</div>
			
			<div class="uk-text-center uk-padding uk-padding-remove-horizontal">
				<span class="uk-text-small uk-text-muted">© 2019 yourMoney - <a href="">Desenvolvido por Lucas Silva| Feito com <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a></span>
			</div>
		</footer>
		</div>
	</div>
</div>


<script src='js/jquery.js'></script>
<script src='js/uikit.js'></script>
<script src='js/parallax.js'></script>
<script>
	$('.parallax-window').parallax();
</script>
<script src='js/uikit-icons.js'></script>
<script src='js/swiper.min.js'></script>
<script type="text/javascript" src="js/jquery.slides.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>
	